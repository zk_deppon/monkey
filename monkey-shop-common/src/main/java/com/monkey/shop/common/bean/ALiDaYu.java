/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.common.bean;

import lombok.Data;

/**
 * 阿里大鱼配置信息
 * @author zkk
 */
@Data
public class ALiDaYu {

	private String accessKeyId;
	
	private String accessKeySecret;
	
	private String signName;
}
