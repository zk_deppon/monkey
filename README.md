一个基于spring boot、spring oauth2.0、mybatis、redis的轻量级、前后端分离、防范xss攻击、拥有分布式锁，为生产环境多实例完全准备，数据库为b2b2c设计，拥有完整sku和下单流程的完全开源商城.


## 前言

`3monkeys`项目致力于为中小企业打造一个完整、易于维护的开源的电商系统，采用现阶段流行技术实现。后台管理系统包含商品管理、订单管理、运费模板、规格管理、会员管理、运营管理、内容管理、统计报表、权限管理、设置等模块。


## 授权

3monkeys官网 http://3monkeys.shop

3monkeys 使用 AGPLv3 开源，请遵守 AGPLv3 的相关条款，或者联系作者获取商业授权(http://3monkeys.shop)

## 演示地址

 **由于我们并不希望小程序的数据被弄混乱，我们弄了两个数据库。因此，您修改了后台的商品信息，小程序并不能看到！** 

后台：<http://admin.3monkeys.shop>  账号：admin/123456

小程序：1. 扫描二维码

![小程序](http://www.qiniuyun.3monkeys.shop/20200720113118.jpg)

​		2. 搜索小程序 **三只猴子平台** 

## 技术选型

| 技术                   | 版本   | 说明                                    |
| ---------------------- | ------ | --------------------------------------- |
| Spring Boot            | 2.1.6  | MVC核心框架                             |
| Spring Security oauth2 | 2.1.5  | 认证和授权框架                          |
| MyBatis                | 3.5.0  | ORM框架                                 |
| MyBatisPlus            | 3.1.0  | 基于mybatis，使用lambda表达式的         |
| Swagger-UI             | 2.9.2  | 文档生产工具                            |
| Hibernator-Validator   | 6.0.17 | 验证框架                                |
| redisson               | 3.10.6 | 对redis进行封装、集成分布式锁等         |
| hikari                 | 3.2.0  | 数据库连接池                            |
| log4j2                 | 2.11.2 | 更快的log日志工具                       |
| fst                    | 2.57   | 更快的序列化和反序列化工具              |
| orika                  | 1.5.4  | 更快的bean复制工具                      |
| lombok                 | 1.18.8 | 简化对象封装工具                        |
| hutool                 | 4.5.0  | 更适合国人的java工具集                  |
| swagger-bootstrap      | 1.9.3  | 基于swagger，更便于国人使用的swagger ui |



## 部署教程

ps: 如果你不清楚如何启动我们的商城，请仔细阅wiki当中的文档




## 相关截图



### 1. 后台截图

![登陆](http://www.qiniuyun.3monkeys.shop/20200720113437.png)

![订单](http://www.qiniuyun.3monkeys.shop/20200720113455.png)

![产品](http://www.qiniuyun.3monkeys.shop/20200720113501.png)

![规格](http://www.qiniuyun.3monkeys.shop/20200720113509.png)

![运费模板](http://www.qiniuyun.3monkeys.shop/20200720113514.png)



### 2. 小程序截图

![小程序截图](http://www.qiniuyun.3monkeys.shop/zuhe.png "小程序截图")

## 提交反馈

- QQ群：895466229



## 后续会加入微服务框架、分布式版本

## 你的点赞鼓励，是我们前进的动力~
## 你的点赞鼓励，是我们前进的动力~
## 你的点赞鼓励，是我们前进的动力~