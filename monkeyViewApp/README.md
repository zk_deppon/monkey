一个基于vue、element ui 的轻量级、前后端分离、拥有完整sku和下单流程的完全开源商城 小程序端



## 前言

`三只猴子商城`项目致力于为中小企业打造一个完整、易于维护的开源的电商系统，采用现阶段流行技术实现。后台管理系统包含商品管理、订单管理、运费模板、规格管理、会员管理、运营管理、内容管理、统计报表、权限管理、设置等模块。


## 授权

3monkeys官网 http://3monkeys.shop

3monkeys官网 使用 AGPLv3 开源，请遵守 AGPLv3 的相关条款，或者联系作者获取商业授权(http://3monkeys.shop)


## 项目链接

java后台：https://gitee.com/zk_deppon/private_3monkeys

vue中后台：https://gitee.com/zk_deppon/private_3monkeys

小程序：https://gitee.com/zk_deppon/private_3monkeys



## 演示地址

 **由于我们并不希望小程序的数据被弄混乱，我们弄了两个数据库。因此，您修改了后台的商品信息，小程序并不能看到！** 

后台：<http://admin.3monkeys.shop>  账号：admin/123456

小程序：1. 扫描二维码

![小程序](http://www.qiniuyun.3monkeys.shop/20200720113118.jpg)

​		2. 搜索小程序 **三只猴子平台** 



## 相关截图

![小程序截图](http://www.qiniuyun.3monkeys.shop/zuhe.png "小程序截图")







## 提交反馈

提问之前，请先阅读[提问的智慧](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way/blob/master/README-zh_CN.md)：

- QQ群：1121081307

 ![输入图片说明](http://www.qiniuyun.3monkeys.shop/20200720113331.png "微信图片_20190723091006.jpg")
