// components/nav-search/index.js
const App = getApp();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
		isCancel: Boolean,
		isBack: Boolean
  },
	options: {
		addGlobalClass: true,
		multipleSlots: true
	},
  /**
   * 组件的初始数据
   */
  data: {
		value: ''
  },

	lifetimes: {
		attached: function() {
			this.setData({
			  navH: App.globalData.navHeight,
			  btnTop: App.globalData.btnTop,
			  btnHeight: App.globalData.btnHeight
			})
		}
	},
  /**
   * 组件的方法列表
   */
  methods: {
		_gotoSearch() {
			if (this.data.isCancel) return
			wx.navigateTo({
				url: '../search-page/search-page'
			})
		},
		change(e) {
			if(!this.data.isCancel) return
			// console.log(e.detail)
			this.triggerEvent('change', e.detail)
		},
		search(e) {
			if(!this.data.isCancel) return
			// console.log(e)
			this.triggerEvent('search', e.detail)
		},
		cacel() {
			// console.log('取消')
			this.setData({
				value: ''
			})
		}
  }
})
