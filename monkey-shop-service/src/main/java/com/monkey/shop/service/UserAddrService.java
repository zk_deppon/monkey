/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.shop.bean.model.UserAddr;

public interface UserAddrService extends IService<UserAddr> {

	UserAddr getDefaultUserAddr(String userId);

	/**
	 * 更新默认地址
	 * @param addrId 默认地址id
	 * @param userId 用户id
	 */
	void updateDefaultUserAddr(Long addrId, String userId);


    void removeUserAddrByUserId(Long addrId, String userId);

    UserAddr getUserAddrByUserId(Long addrId, String userId);
}

