/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.service;

import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.shop.bean.enums.SmsType;
import com.monkey.shop.bean.model.SmsLog;

/**
 *
 * @author zkk on 2018/11/29.
 */
public interface SmsLogService extends IService<SmsLog> {

	public void sendSms(SmsType smsType,String userId,String mobile,Map<String,String> params);
	
	public boolean checkValidCode(String mobile, String code,SmsType smsType);
}
