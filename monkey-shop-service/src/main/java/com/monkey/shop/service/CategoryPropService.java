/*
 * Copyright (c) 2018-2999 武汉三只猴子科技有限公司 All rights reserved.
 *
 * https://www.3monkeys.shop/
 *
 * 未经允许，不可做商业用途！
 *
 * 版权所有，侵权必究！
 */

package com.monkey.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.monkey.shop.bean.model.CategoryProp;

/**
 *
 * Created by zkk on 2018/07/13.
 */
public interface CategoryPropService extends IService<CategoryProp> {

}
